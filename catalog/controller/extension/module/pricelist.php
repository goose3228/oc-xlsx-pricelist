<?php
class ControllerExtensionModulePricelist extends Controller {
	public function index() {
        $this->browser_output("Pricelist " . date("Y-m-d"));
	}

    private function browser_output($name = "pricelist") {
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$name.'.xlsx"');
        header('Cache-Control: max-age=0');
        header('Cache-Control: max-age=1');
        header('Expires: Mon, 26 Jul 1997 00:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0

        fpassthru(fopen(DIR_SYSTEM . "library/pricelist/out/pricelist.xlsx", "rb"));
        exit();
    }
}
?>

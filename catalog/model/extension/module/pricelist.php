<?php
require_once(DIR_APPLICATION . "model/catalog/product.php");

class ModelExtensionModulePricelist extends ModelCatalogProduct {
    public function products() {
        $this->load->model("catalog/product");
        $products = $this->model_catalog_product->getProducts();

		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product_attribute WHERE attribute_id = 30");
        foreach($query->rows as $row)
            if(isset($products[$row["product_id"]]))
                $products[$row["product_id"]]["material"] = $row["text"];


        $query = $this->db->query("SELECT C.category_id, C.name category_name, P.product_id FROM ".DB_PREFIX."category_description C INNER JOIN ".DB_PREFIX."product_to_category P ON C.category_id = P.category_id WHERE language_id = 1 GROUP BY product_id");
        $chash = [];
        foreach($query->rows as $row)
            $chash[$row["product_id"]] = $row;

        foreach($products as &$p) {
            $p["width"] = (float) $p["width"];
            $p["height"] = (float) $p["height"];
            $p["length"] = (float) $p["length"];
            $p["price"] = (float) $p["price"];
            $p["size"] = ($p["length"] > 0) ? intval($p["length"]) . "x" . intval($p["width"]) . "x" . intval($p["height"]) : null;
            if(isset($chash[$p["product_id"]])) {
                $p["category_name"] = $chash[$p["product_id"]]["category_name"];
                $p["category_id"] = $chash[$p["product_id"]]["category_id"];
            }
        }

        return $products;
    }

    public function categories() {
        $categories = [];
        foreach($this->products() as $p) {
            if(empty($p["category_name"])) continue;
            if($p["quantity"] < 10) continue;
            if(empty($categories[$p["category_name"]])) $categories[$p["category_name"]] = [];
            $categories[$p["category_name"]][] = $p;
        }
        return $categories;
    }
}
?>

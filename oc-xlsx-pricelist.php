<?php
use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Style\Font;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Alignment;
use PhpOffice\PhpSpreadsheet\Style\Style;

require_once __DIR__ . '/vendor/phpoffice/phpspreadsheet/src/Bootstrap.php';

class OCXLSXPricelist {
    public $categories = [];
    public $cache_time = 86400;
    public $header_height = 34.5;
    public $row_height = 50.1;
    public $image_scale = 1.25;
    public $base_url = HTTP_SERVER . "?route=product/product&product_id=";

    public $columns = [];

    public $header_style = [
        "font" => [
            "bold" => true,
        ],
        "fill" => [
            'fillType' => Fill::FILL_SOLID,
            'color' => ['rgb' => 'C6D9F1']
        ],
        "alignment" => [
            "horizontal" => Alignment::HORIZONTAL_CENTER,
            "vertical" => Alignment::VERTICAL_CENTER
        ],
        'borders' => [
            'bottom' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "E0E0E0E0"]
            ],
            'right' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "E0E0E0E0"]
            ],
        ],
    ];

    public $base_style = [
        'borders' => [
            'bottom' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "00FFFFFF"]
            ],
            'right' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "00FFFFFF"]
            ],
        ],
    ];

    public $data_style = [
        'borders' => [
            'bottom' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "E0E0E0E0"]
            ],
            'right' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "E0E0E0E0"]
            ],
        ],
        "alignment" => [
            "vertical" => Alignment::VERTICAL_CENTER
        ]
    ];

    public $link_style = [
        "font" => [
            "underline" => Font::UNDERLINE_SINGLE,
            "color" => ["argb" => "FF0000E0"]
        ],
        'borders' => [
            'bottom' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "E0E0E0E0"]
            ],
            'right' => [
                'borderStyle' => Border::BORDER_HAIR,
                "color" => ["argb" => "E0E0E0E0"]
            ],
        ],
        "alignment" => [
            "vertical" => Alignment::VERTICAL_CENTER
        ]
    ];

    private $spreadsheet;
    private $sheet;

    public function __construct() {
        $this->spreadsheet = new Spreadsheet();
    }

    private $col_names = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"];
    private function col_name($index) {
        return $this->col_names[$index];
    }
    private function col_index($name) {
        return array_search($name, $this->col_names);
    }

    public function make($output) {
        if(filemtime($output) + $this->cache_time > time()) return;

        $i = 0;
        foreach($this->categories as $c => $products) {
            if($i > 0) $this->spreadsheet->createSheet();
            $this->sheet = $this->spreadsheet->setActiveSheetIndex($i);
            $this->sheet->setTitle($c);
            $this->write_sheet($products);
            $i++;
        }
        $this->spreadsheet->setActiveSheetIndex(0);

        $writer = IOFactory::createWriter($this->spreadsheet, 'Xlsx');
        $writer->save($output);
    }

    private function write_sheet($products) {
        $data_height = count($products) + 1;
        $data_width = $this->col_name(count($this->columns) - 1);
        $sheet_height = $data_height + 50;

        $this->apply_style($this->base_style, "A1:Z{$sheet_height}");
        $this->apply_style($this->data_style, "A2:{$data_width}{$data_height}");
        $this->write_header();

        $i = 2;
        foreach($products as $p) {
            $this->write_row($p, $i);
            $i++;
        }

        foreach($this->columns as $i => $c) {
            if(empty($c["link"])) continue;
            $cn = $this->col_name($i);
            $this->apply_style($this->link_style, "{$cn}2:{$cn}{$data_height}");
        }

        $this->sheet->setAutoFilter("A1:{$data_width}{$data_height}");
    }

    private function write_row($p, $row) {
        for($i = 0; $i < count($this->columns); $i++) {
            $col = $this->columns[$i];
            if(empty($col["name"])) $col["name"] = $this->col_name($i);
            if(isset($p[$col["key"]])) {
                $cell = $col["name"] . $row;
                if(!empty($col["image"])) $this->write_image($cell, $p[$col["key"]]);
                else {
                    if(!empty($col["link"])) $this->write_link($cell, $p[$col["key"]]);
                    else {
                        if(!empty($col["currency"])) $this->sheet->getCell($cell)->setValue(number_format($p[$col["key"]], 2, ".", " ") . " " . $col["currency"]);
                        else $this->sheet->setCellValue($cell, $p[$col["key"]]);
                    }
                }
                if(isset($col["align"]))
                    $this->sheet->getCell($cell)->getStyle()->getAlignment()->setHorizontal($col["align"]);
            }
        }
        $this->sheet->getRowDimension($row)->setRowHeight($this->row_height);
    }

    private function write_header() {
        $this->sheet->getRowDimension(1)->setRowHeight($this->header_height);
        for($i = 0; $i < count($this->columns); $i++) {
            $col = $this->columns[$i];
            if(empty($col["name"])) $col["name"] = $this->col_name($i);
            if(!empty($col["width"])) $this->sheet->getColumnDimension($col["name"])->setWidth($col["width"]);
            if(!empty($col["label"])) $this->sheet->setCellValue($col["name"] . "1", $col["label"]);
            $range = "A1:" . $col["name"] . "1";
        }

        $this->apply_style($this->header_style, $range);
    }

    private function apply_style($array, $range) {
        $style = new Style();
        $style->applyFromArray($array);
        $this->sheet->duplicateStyle($style, $range);
    }

    private function write_image($cell, $image) {
        $thumb = $this->get_img_thumb($image);
        if(!$thumb) return;
        $drawing = new \PhpOffice\PhpSpreadsheet\Worksheet\MemoryDrawing();
        $drawing->setImageResource($thumb);
        $drawing->setCoordinates($cell);
        $drawing->setHeight($this->row_height * $this->image_scale);
        $drawing->setOffsetX(20);
        $drawing->setWorksheet($this->sheet);
    }

    private function write_link($cell, $link) {
        $url = $this->base_url . $link;
        $this->sheet->getCell($cell)->setValue($url)->getHyperlink()->setUrl($url);
    }

    private function get_img_thumb($img) {
        if(empty($img)) return;
        $filename = DIR_IMAGE . $img;
        if(!file_exists($filename)) return;

        $width = 100;
        $height = 100;
        list($width_orig, $height_orig) = getimagesize($filename);
        $ratio_orig = $width_orig / $height_orig;
        if ($width / $height > $ratio_orig) {
            $width = $height * $ratio_orig;
        } else {
            $height = $width / $ratio_orig;
        }
        $image_p = imagecreatetruecolor($width, $height);
        $image = imagecreatefromjpeg($filename);
        imagecopyresampled($image_p, $image, 0, 0, 0, 0, $width, $height, $width_orig, $height_orig);
        return $image_p;
    }
}

?>

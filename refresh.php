<?php
if(isset($_SERVER['argv']) && isset($_SERVER['argv'][1])) $config_path = $_SERVER['argv'][1];
else $config_path = "../../../config.php";

touch(__DIR__ . "/out/debug.log");

function l($msg) {
    file_put_contents(__DIR__ . "/out/debug.log", file_get_contents(__DIR__ . "/out/debug.log") . "\n" . $msg . "\n");
}

try {
    define('VERSION', '3.0.3.2');

    require_once($config_path);
    require_once(DIR_SYSTEM . 'startup.php');
    require_once(DIR_APPLICATION . "model/extension/module/pricelist.php");

    $registry = new Registry();

    $config = new Config();
    $config->load('default');
    $config->load('catalog');

    $registry->set('config', $config);
    $registry->set('load', new Loader($registry));
    $registry->set('request', new Request());
    $registry->set('event', new Event($registry));
    $registry->set('db', new DB($config->get('db_engine'), $config->get('db_hostname'), $config->get('db_username'), $config->get('db_password'), $config->get('db_database'), $config->get('db_port')));

    $config->set('config_customer_group_id', 1);
    $config->set('config_language_id', 1);
    $m = new ModelExtensionModulePricelist($registry);

    require_once(__DIR__ . "/make.php");

    make_pricelist([
        "output" => __DIR__ . "/out/pricelist.xlsx",
        "categories" => $m->categories()
    ]);
}
catch (Exception $e) {
    echo $e->getMessage() . "\n";
    echo $e->getTraceAsString() . "\n";
    l(print_r($e, true));
}
?>

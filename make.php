<?php
require_once __DIR__ . "/oc-xlsx-pricelist.php";

function make_pricelist($config) {
    $pl = new OCXLSXPricelist();
    $pl->categories = $config["categories"];
    $pl->cache_time = 0;

    $pl->columns = [
        [
            "label" => "Image",
            "key" => "image",
            "image" => true,
            "width" => 10,
        ],
        [
            "label" => "Name",
            "key" => "name",
            "width" => 50
        ],
        [
            "label" => "Link",
            "key" => "product_id",
            "link" => true,
            "width" => 60
        ]
    ];

    $pl->make($config["output"]);
}
?>
